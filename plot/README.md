---
title: Plots
---

This folder contains the necessary scripts needed to generate the relevant plots in the paper.

## Dependencies

We use [`R`](https://www.r-project.org/) to compile our data with the following library dependencies:

* ggplot2
* reshape2
* dplyr
* xtable  

You can run the following to install dependencies if you have R already:

```
sudo make dependencies
```

## Plotting the Graphs

The [plot.r](plot.r) is used to generate the two graphs present in the paper.
Data used to generate the graphs can be found in [data/data.csv](data/data.csv).

```
make
```

## Cleaning up

Use `make clean` to remove all generated files.
