# Common Plotting
source("common.r")

# Set up graphs
cols<- "Set1"
leg <- "none"

# Read data and process it by doing basic replacements and values ordering
dat<-read.csv("data/data.csv", header = TRUE)

# Tidy Algorithms
dat$prog <- factor(dat$prog, 
                  levels=c("refrun", "ajrun","multinew"), 
                  labels=c("Non-Monitored", "Global", "Opportunistic"))


res <- dat %>% 
  mutate(threads=readers) %>%
  group_by(prog, advice, csize) %>%

  summarize(
    mean_exec = mean(exec),
    sd_exec = sd(exec)
  )

res <- dat %>% 
  mutate(threads=readers+writers) %>%
  group_by(prog, advice, threads, csize) %>%
  summarize(
    mean_exec = mean(exec),
    sd_exec = sd(exec)
  )


labelInfo <-
  split(dat, dat$prog) %>%
  lapply(function(x){
    data.frame(
      predAtMax = lm(exec~csize, data=x) %>%
        predict(newdata = data.frame(csize = max(x$csize)))
      , max = max(x$csize)
    )}) %>%
  bind_rows

labelInfo$label = levels(dat$prog)

lmestimatecz <- function(df, ag) {
  return (split(df, df$prog) %>%
    lapply(function(x){
      data.frame(
        lmvalue = lm(exec~csize, data=x) %>%
          predict(newdata = data.frame(csize = ag(x$csize)))
        , at = ag(x$csize)
      )}) %>%
    bind_rows)
}

levels(dat$prog)
lmmin <- lmestimatecz(dat, min)
lmmax <- lmestimatecz(dat, max)
lmmin$label = levels(dat$prog)
lmmax$label = levels(dat$prog)


gdraw(ggplot(dat, aes(x=csize, y=exec, color=prog)) + 
#        geom_jitter(width=0.015) +
        geom_point() +
        scale_y_log10(breaks=c(1000, 1500, 2000, 2500, 3000,4000)) +
        geom_smooth(method="lm", se=FALSE) +
        scale_color_brewer(palette = cols, name="Approach")+
        ylab("Execution Time (ms logscale)")+
        xlab("Concurrency Region Size (events)") +
#        facet_grid(. ~ advice) +
        scale_x_continuous(breaks=unique(dat$csize),  trans = "log10")) +  
#  geom_label(data = lmmax
#             , aes(x= at
#                   , y = lmvalue
#                   , label = round(lmvalue)
#                   , color = label)) +
  ggsave("cwidth.pdf")

dthreads <- dat %>% mutate(threads=readers)

lmestimatenr <- function(df, ag) {
  return (split(df, df$prog) %>%
            lapply(function(x){
              data.frame(
                lmvalue = lm(exec~threads, data=x) %>%
                  predict(newdata = data.frame(threads = ag(x$threads)))
                , at = ag(x$threads)
              )}) %>%
            bind_rows)
}
levels(dthreads$prog)
lmestimatenr(dthreads, min)
lmestimatenr(dthreads, max)


gdraw(ggplot(dthreads, aes(x=threads, y=exec, color=prog)) + 
        geom_point() +
        scale_y_log10(breaks=c(1000, 1500, 2000, 2500, 3000,4000)) +
        geom_smooth(method="lm", se=FALSE) +
        scale_color_brewer(palette = cols, name="Approach")+
        ylab("Execution Time (ms logscale)")+
        xlab("Number of Readers") +
#        facet_grid(. ~ advice) +
        scale_x_continuous(breaks=unique(dthreads$threads), trans = "log10")) +  
  ggsave("nreaders.pdf")

simpled <- dthreads %>% filter(csize==60)
gdraw(ggplot(simpled, aes(x=threads, y=exec, color=prog)) + 
        geom_point() +
        scale_y_log10(breaks=c(1000, 1500, 2000, 2500, 3000,4000)) +
        geom_smooth(method="lm", se=FALSE) +
        scale_color_brewer(palette = cols, name="Approach")+
        ylab("Execution Time (ms logscale)")+
        xlab("Number of Readers") +
#                facet_grid(. ~ advice) +
        scale_x_continuous(breaks=unique(simpled$threads), trans = "log10")) +  
  ggsave("csize60.pdf")




