# R package installer

# Set default mirror
# Well it is possible to find a better one
local({r <- getOption("repos")
       r["CRAN"] <- "http://cran.r-project.org" 
       options(repos=r)
})

# List needed packages
pkgs <- c("stringi", "ggplot2", "dplyr")

# Filter out already installed packaged
#inst <- pkgs[!(pkgs %in% installed.packages()[,"Package"])]

# Install whatever is needed 
#if(length(inst)) install.packages(inst)
install.packages(pkgs)
