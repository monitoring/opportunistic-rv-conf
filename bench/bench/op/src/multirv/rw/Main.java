package multirv.rw;

import multirv.Harness;
import multirv.monitor.MonitorLayer;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        int nWriters = Integer.parseInt(args[0]);
        int nReaders = Integer.parseInt(args[1]);
        int nWrites = Integer.parseInt(args[2]);
        int nReads = Integer.parseInt(args[3]);
        int arraySize = Integer.parseInt(args[4]);
        int cReads = Integer.parseInt(args[5]);
        SharedVar var = new SharedVar(arraySize);
        
		Integer[] lwrite = Harness.balance(nWrites, nWriters);
		Integer[] lread  = Harness.balance(nReads/cReads, nReaders);

        Runnable[] runs = new Runnable[nReaders + nWriters];
        int i;
        for(i = 0; i < nWriters; i++)
            runs[i]     = new Writer(var, lwrite[i]);

        for(int j = 0; j < nReaders; j++)
            runs[i + j] = new Reader(var, lread[j], cReads);

        MonitorLayer.init(nReaders);

        Harness bench = new Harness(nReaders + nWriters);
        bench.start(runs);
        bench.end();
    }

}
