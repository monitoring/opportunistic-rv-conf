package multirv.rw;




import multirv.monitor.Events;
import multirv.monitor.MonitorLayer;
import multirv.monitor.MonitorLocal;
import multirv.monitor.MonitorScope;

import java.util.Random;
public class Reader implements Runnable {

	private SharedVar var;
	private int nReads = 0;
	private int cReads = 1;
	private Random r = new Random();

	public Reader(SharedVar var, int nReads, int cReads) {
		this.var = var;
		this.nReads = nReads;
		this.cReads = cReads;
	}

	@Override
	public void run() {
		MonitorLocal mon = MonitorLayer.spawn(Events.READ);

		try {
			for(int i = 0; i < nReads; i++) {

				var.readtry.acquire();
				var.rmutex.acquire();
				var.RC++;

				if(var.RC == 1) {
                    var.resource.acquire();
                    MonitorScope.sync();
                }
				var.rmutex.release();
				var.readtry.release();

				for(int k = 0; k < cReads; k++) {
					mon.onEvent(Events.READ);
                    var.readArr();
                }

				var.rmutex.acquire();
				var.RC--;


				if(var.RC == 0)
					var.resource.release();
				var.rmutex.release();

			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
