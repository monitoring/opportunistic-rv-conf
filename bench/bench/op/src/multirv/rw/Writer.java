package multirv.rw;


import multirv.monitor.Events;
import multirv.monitor.MonitorLayer;
import multirv.monitor.MonitorLocal;
import multirv.monitor.MonitorScope;

import java.util.Random;

public class Writer implements Runnable {
	private SharedVar var;
	private int nWrites;
	Random r = new Random();

	public Writer(SharedVar var, int nWrites) {
		this.var = var;
		this.nWrites = nWrites;
	}

	@Override
	public void run() {
		try {
			MonitorLocal mon = MonitorLayer.spawn(Events.WRITE);


			for (int i = 0; i < nWrites; i++) {

                var.wmutex.acquire();
                var.WC++;
                if(var.WC == 1)
                    var.readtry.acquire();
                var.wmutex.release();

                var.resource.acquire();
				MonitorScope.sync();


				mon.onEvent(Events.WRITE);
                var.writeArr(1);


                var.resource.release();

                var.wmutex.acquire();
                var.WC--;
                if(var.WC == 0)
                    var.readtry.release();
                var.wmutex.release();

			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
