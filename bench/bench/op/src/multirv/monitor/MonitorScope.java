package multirv.monitor;


public class MonitorScope {
    private static MonitorData[] data;
    private static int timestamp = 0;
    private static int readers;


    public static Integer getTimestamp() {
        return timestamp;
    }

    public static void init(MonitorData[] data, int nreaders) {
        MonitorScope.data       = data;
        MonitorScope.readers    = nreaders;
    }

    public static void sync() {
        int nreaders = data[0].size();
        int nwriters = data[1].size();
        Boolean verdict = true;

        if(nreaders > 0 && nwriters > 0)
            verdict = false;
        else if(nwriters > 0 && (nwriters != 1))
            verdict = false;
        else if(nreaders != readers)
            verdict = false;

        data[0].clear();
        data[1].clear();
        timestamp++;
        //if(verdict == false) {
        //   System.err.println("Property Failed");
        //    Runtime.getRuntime().exit(1);
        //}
    }
}
