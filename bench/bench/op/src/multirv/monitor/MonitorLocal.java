package multirv.monitor;


public class MonitorLocal {
    private MonitorData data;
    private int localTimestamp = 0;
    private Events type;
    boolean hasSeen = false;

    public MonitorLocal(MonitorData data, Events event) {
        this.data = data;
        this.type = event;
        localTimestamp = 0;
    }
    public void onEvent(Events event) {
        int ts = MonitorScope.getTimestamp();
        if(localTimestamp < ts) { //Detect Sync -> Reset
            localTimestamp = ts;
            hasSeen = false;
        }
        if(event == type) {
            if(!hasSeen) { // Spec is:  F(event)
                hasSeen = true;
                data.put(hashCode(), true);
            }
        }
    }


}
