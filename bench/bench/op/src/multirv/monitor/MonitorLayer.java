package multirv.monitor;


public class MonitorLayer {
    private static MonitorData data[] = new MonitorData[] {
            new MonitorData(), new MonitorData()
    };

    public static void init(int nreaders) {
        MonitorScope.init(data, nreaders);
    }

    public static MonitorLocal spawn(Events type) {
        switch(type) {
            case READ:
                return new MonitorLocal(data[0], Events.READ);
            case WRITE:
                return new MonitorLocal(data[1], Events.WRITE);
            default:
                throw new Error("Monitor type unrecognized: " + type);
        }
    }
}
