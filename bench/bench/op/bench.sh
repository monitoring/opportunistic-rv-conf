#!/bin/bash

READERS=3
WRITERS=1
NW=100000
NR=400000
SIZE=10000

concur=(1 5 10 15 30 60 100 150) 
readers=(1 3 7 15 23 31 63 127)
writers=(1 1 1 1 1 1 1 1)


for NC in "${concur[@]}"; do
for i in $(seq 1 $1); do
	echo "#### $NC - $i #####"
	for j in $(seq 0 7); do
	READERS="${readers[j]}"
	WRITERS="${writers[j]}"
	v=$(make multi DRIVER="multirv.rw.Main $WRITERS $READERS $NW $NR $SIZE $NC" | grep "main Complete" | cut -d" " -f 4)
	echo "$2,multinew,$WRITERS,$READERS,$NW,$NR,$SIZE,$NC,$v" | tee -a $2-multi.log
	done
done
done
