import java.util.concurrent.Semaphore;

public class SharedVar {
	private int[] arr;
    public Semaphore resource;
	public Semaphore rmutex;
	public Semaphore wmutex;
	public Semaphore readtry;
	public int WC = 0;
	public int RC = 0;


	public SharedVar(int size) {
		arr = new int[size];
        resource = new Semaphore(1);
        rmutex   = new Semaphore(1);
		wmutex   = new Semaphore(1);
		readtry  = new Semaphore(1);
	}

	public int readArr() {
		int sum = 0;
		for (int i = 0; i < arr.length; i++)
			sum += arr[i];
		return sum;
	}

    public void writeArr(int x) {
        for (int i = 0; i < arr.length; i++)
            arr[i] = x;
    }

}
