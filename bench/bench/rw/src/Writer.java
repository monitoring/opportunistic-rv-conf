import java.util.Random;

public class Writer implements Runnable {
	private SharedVar var;
	private int nWrites;
	Random r = new Random();

	public Writer(SharedVar var, int nWrites) {
		this.var = var;
		this.nWrites = nWrites;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i < nWrites; i++) {

					var.wmutex.acquire();
					var.WC++;
					if(var.WC == 1)
						var.readtry.acquire();
					var.wmutex.release();

					var.resource.acquire();
					var.writeArr(1);
					var.resource.release();

					var.wmutex.acquire();
					var.WC--;
					if(var.WC == 0)
						var.readtry.release();
					var.wmutex.release();

			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
