import java.io.*;
import java.util.*;

public aspect RWMonitor {

    private static RWRuntimeMonitor mon = new RWRuntimeMonitor();

    before(String[] a) : execution(* Driver.main(String[])) && args(a) {
        RWRuntimeMonitor.nReaders = Integer.parseInt(a[1]);
    }

   before() : call(int SharedVar.readArr())
   {
        synchronized (mon) {
            mon.readEvent();
        }
   }

   before() : call(void SharedVar.writeArr(int))
   {
        synchronized (mon) {
            mon.writeEvent();
        }
   }

   public static class RWRuntimeMonitor {
        private static int nReaders = 2;
        private Set<String> threads = new HashSet<>();

        public  void readEvent() {
            threads.add(Thread.currentThread().getName());
        }
        public void writeEvent() {
            if(!threads.isEmpty()) {
                if (threads.size() != nReaders) {
                    //System.err.println("Not all readers have read the value:" + threads.size() + "/" + nReaders);
                }
            }
            threads.clear();
        }
   }
}
