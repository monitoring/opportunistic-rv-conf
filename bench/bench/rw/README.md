---
title: RW-Preference (AspectJ)
---

## Readers-Writers with writer preference using locks and AspectJ monitoring

### Overview

The program class and arguments are as follows:

```
Driver <nwriters> <nreaders> <nwrites> <nreads> <size> <cwidth>
```

1. `nwriters`: number of writer threads
2. `nreaders`: number of reader threads
3. `nwrites`: number of writes performed (balanced among writers)
4. `nreads`: number of reads performed (balanced among readers)
5. `size`: size of the array to write to/read from
6. `cwidth`: number of reads in one concrrency region

See [driver](driver) for an example execution


### Executing the Base Program

[Optional] To compile the program use:

```
make refcompile
```

To execute the program use:

```
make refrun
```

To remove compiled files:

```
make refclean
```

### Executing with Global Monitoring (AspectJ)

[Optional] To compile the program you need to have the AspectJ Compiler installed (ajc).


```
make ajcompile SPEC=RWMonitor.aj
```

To execute the program use:

```
make ajrun
```

### Benchmarking

The [bench.sh](bench.sh) script is used to benchmark the program. It is used as follows:

```
./bench.sh <n> <key> <target>
```

Where:

1. `n` is the number of executions for each parameters (edit the files for the parameters)
2. `key` is a string identifier used as a tag for executions (for analyzing the data later)
3. `target` is either `refrun` for the base program or `ajrun` for the global monitored program.
