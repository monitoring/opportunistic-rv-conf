---
title: RW-Preference
---

## Readers-Writers with writer preference preliminary overhead assessment

### Versions

We present two versions of readers-writers:

1. [bench/rw](bench/rw) Base program along with instrumentation for  using global monitoring (AspectJ)
2. [bench/op](bench/op) Instrumented program monitored using opportunistic RV

Check each for details on how to compile and execute.

### Benchmarking

The [bench.sh](bench.sh) script is used to run the full benchmark, edit it to change benchmark parameters.

> Note: executing the benchmark may take some time due to the number of parameter permutations. For testing make sure to set N small in the script, or simply go to each program and check its own benchmark script or execution script.

Then execute using:

```
./bench.sh
```

You can find all the data already generated in [../plot/data](../plot/data).
