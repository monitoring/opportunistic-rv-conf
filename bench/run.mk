AJCP?=../../aspectjrt.jar
HARN?=harness

# Defaults
DRIVER?=${shell cat driver}

# Parameters
N?=50
TOOL?=ref

.PHONY: default scenario pclean clean refclean refcompile refrun ajcompile ajrun ajclean

default: scenario

# Default program
refclean:
	@rm -rf original 2>/dev/null

refcompile: refclean
	mkdir original
	javac -cp ${HARN}:original/ -d original src/*

refrun:
	java -cp original/ ${DRIVER}

# Arbitrary AspectJ
ajcompile: ajclean
	@mkdir aj
	ajc -showWeaveInfo -cp ${AJCP} \
 		-1.8 -sourceroots ${HARN}:src -d aj \
		${SPEC}
ajrun:
	java -cp ${AJCP}:aj/ ${DRIVER}

ajclean:
	make -s pclean WDIR=aj

# Remove a working folder
pclean:
	-@rm -rf ${WDIR} 2>/dev/null

# Clean All
clean: refclean ajclean 
	-rm -rf *.log *.1k *.10k 2>/dev/null
	@(test -f clean.sh && ./clean.sh) || true

# Default
scenario:
	${info See documentation for details}
