---
title: Artifact Repostiory - Opportunistic RV of Multithreaded Programs
---

## Downloading the artifact

To download the artifact simply clone the repository.

```
git clone https://gitlab.inria.fr/monitoring/opportunistic-rv-conf.git
```

## Reading the README.md Files ##
All README.md files are pre-rendered as HTML for convenience.
In each directory (including this one), the `index.html` file corresponds to a rendered `README.md`.


## Exploring the Artifact
The artifact contains two folder relevant to the paper:

* [plot](plot): contains data and scripts used to generates the plots, and
* [bench](bench): contains the programs executed to generate the data that was plotted, including scripts to build and run them.


We provide **both** the data and the necessary steps to re-create it.
