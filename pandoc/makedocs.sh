#!/bin/bash

function build_doc {
  files=($(find ../ -type f -name 'README.md'))
  for file in ${files[*]}
  do
    css=$(echo $file | sed 's,[^\/]*/,../,g' | sed 's,README.md,pandoc/github.css,g' | sed 's,^...,,g')
    out=$(echo $file | sed 's,README.md,index.html,g')
    printf "%s > %s \n" $file $out
    pandoc -c $css -s -f markdown+smart $file -o $out
  done
}
function delete_doc {
  files=($(find ../ -type f -name 'README.md'))
  for file in ${files[*]}
  do
    out=$(echo $file | sed 's,README.md,index.html,g')
    printf "X %s\n" $out
    rm $out
  done
}


case "$1"
in
"make") build_doc;;
"delete") delete_doc;;
"") echo "Use 'make' or 'delete' to generate or delete documentation";;
esac


